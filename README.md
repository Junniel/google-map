# README #


### How to use Google Maps> ###

* Here's a guide for you on how to use Google Map

Google Mapsis one of the most widely used services nowadays.
It is an incredibly versatile tool which is quick and easy to handle. 
Starting from measuring the distance between any two locations to providing directions to the drivers, bikers, walkers, and users of public transportation it helps users in a variety of ways.

Google Maps have made out life much easy and now it’s difficult to imagine a life without it. 
Do you know how to use it? Here is a step-by-step instruction on how to use Google Maps.


### Here's a guide to get starting Google Maps###

Step 1: At first you need to open the Google Maps app.

Step 2: Search for a place or tap it on the map.

Step 3: In the bottom right, tap directions. (You can even add the destinations)

Step 4: To add destination you have to go to the top right and tap more and then add a stop.

Step 5: Choose one of the following:

Driving
Transit
Walking
Ride Services
Cycling
Step 6: If other routes are available, they will be shown in gray on the map. To follow an alternate route, tap the grayline.

Step 7: To start navigation, tap Start.

Step 8: To stop or cancel navigation, go to the bottom left and tap Close

You can even access the hear voice directions. So that when you navigate to a place, you can hear voice directions.

### How To change the volume? ###
* While using Google maps you can change the volume level. Here are the steps you need to follow:

Step 1: Open the 'Google Maps' app.

Step 2: Tap 'Menu' and then 'Settings'.

Step 3: Tap on 'Navigation settings' and then 'Voice level'.

Step 4: Choose 'Louder, Normal, or Softer'.

### Installation ###
Step 1: Download a map

 Note: You can save maps on your device or an SD card. 
If you change the way you save maps, you’ll have to download the map again.


	On your Android phone or tablet, open the Google Maps app Maps.
Make sure you're connected to the internet and signed in to Google Maps.
Search for a place, like San Francisco.
At the bottom, tap the name or address of the place and then Download Download and then Download. If you searched for a place like a restaurant, tap More More and then Download offline map and then Download.
Save offline maps on an SD card
Offline maps are downloaded on your device’s internal storage by default, but you can download them on an SD card instead. If your device is on Android 6.0 or higher, you can only save an area to an SD card that's configured for portable storage. To learn how to configure your SD card, get help from your phone's manufacturer.

	On your Android phone or tablet, insert an SD card.
Open the Google Maps app Maps.
Tap your profile picture or initial Account Circle and then Offline maps.
In the top right, tap Settings Settings.
Under "Storage preferences," tap Device and then SD card.
Step 2 (optional): Save battery and mobile data
You'll use offline maps, but can still use other apps with mobile data.

	On your Android phone or tablet, open the Google Maps app Maps.
Tap your profile picture or initial Account Circle and then Settings Settings and then turn on Wi-Fi only.
Notes:

You can get driving directions offline, but not transit, bicycling, or walking directions.
You won't have traffic info, alternate routes, or lane guidance.
Use offline maps
After you download an area, use the Google Maps app just like you normally would.

Get directions and see routes
Use navigation
Search for locations
If your Internet connection is slow or absent, Google Maps will use your offline maps to give you directions.

Notes:

You can get driving directions offline, but not transit, bicycling, or walking directions. In your driving directions, you won't have traffic info, alternate routes, or lane guidance.
To save cell data and battery life, use "Wi-Fi only" mode. In this mode, when you’re not connected to Wi-Fi, Google Maps will only use data from the offline maps that you’ve downloaded. Before you use this mode, make sure you download offline maps. To turn on this mode, tap your profile picture or initial Account Circle and then Settings Settings and then turn on Wi-Fi only.